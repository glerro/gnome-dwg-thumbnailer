/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of Gnome Dwg Thumbnailer.
 * https://gitlab.gnome.org/glerro/gnome-dwg-thumbnailer
 *
 * gnome-dwg-thumbnailer.c
 *
 * Copyright (c) 2023 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Gnome Dwg Thumbnailer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gnome Dwg Thumbnailer is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Gnome Dwg Thumbnailer. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023 Gianni Lerro <glerro@pm.me>
 */

#include <stdlib.h>
#include <glib.h>
#include <gio/gio.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

/* Convert from BigEndian to host endianness */
static guint32
get_guint32 (const gchar *data)
{
    guint32 *ptr = (guint32 *) (data);
    return GUINT32_FROM_BE (ptr[0]);
}

/* Try to get a valid PNG Thumbnail. */
gboolean
get_png_thumbnail(GFileInputStream *input_file_stream,
                  guint             png_offset,
                  char            **data,
                  gsize            *data_length,
                  GError          **error)
{
    const guchar png_signature[8] = {0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A};
                                    /* .     P     N     G     .     .     .     . */
    const guchar iend_chunk[4] = {0x49, 0x45, 0x4E, 0x44};
                                 /* I     E     N     D */
    guchar png_header[8], chunk_type[4];
    gchar *chunk_data_length_be;
    guint32 chunk_data_length;
    gsize png_stream_lenght = 0;

    /* File Header:  A PNG file starts with an 8 bytes signature:
     * Check if the file contain the PNG header
     */
    g_seekable_seek (G_SEEKABLE (input_file_stream), png_offset, G_SEEK_SET, NULL, error);
    if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), &png_header, sizeof (png_header), NULL, error) < 0)
    {
        return FALSE;
    }
    if (memcmp (png_signature, png_header, sizeof(png_signature)) != 0)
    {
        return FALSE;
    }

    /* Add the Header length (8 bytes) */
    png_stream_lenght = +8;

    /* Chunks: After the header, comes a series of chunks, each of which conveys certain information about the image.
     * A chunk consists of four parts:
     * 1) Chunk Data Length (4 bytes, big-endian);
     * 2) Chunk Type        (4 bytes);
     * 3) Chunk Data        (Length bytes);
     * 4) Chunk CRC         (4 bytes).
     */
    do
    {
        /* Read the Chunk Data Length (4 bytes) */
        png_stream_lenght += 4;
        chunk_data_length_be = g_malloc (4);
        if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), chunk_data_length_be, 4, NULL, error) < 0)
        {
            return FALSE;
        }
        chunk_data_length = get_guint32 (chunk_data_length_be);
        g_free (chunk_data_length_be);

        /* Read the Chunk Type (4 bytes) */
        png_stream_lenght += 4;
        if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), &chunk_type, 4, NULL, error) < 0)
        {
            return FALSE;
        }

        /* Skip the Chunk Data and CRC (Data Length + 4 bytes) */
        png_stream_lenght += chunk_data_length + 4;
        if (g_input_stream_skip (G_INPUT_STREAM (input_file_stream), chunk_data_length + 4, NULL, error) < 0)
        {
            return FALSE;
        }
    }
    while (memcmp (chunk_type, iend_chunk, sizeof(chunk_type)) != 0);

    /* Read the PNG image data */
    gchar *png_stream = g_malloc (png_stream_lenght);
    g_seekable_seek (G_SEEKABLE (input_file_stream), png_offset, G_SEEK_SET, NULL, error);
    if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), png_stream, png_stream_lenght, NULL, error) < 0)
    {
        return FALSE;
    }

    g_input_stream_close (G_INPUT_STREAM (input_file_stream), NULL, NULL);
    g_object_unref (input_file_stream);

    *data_length = png_stream_lenght;
    *data = png_stream;

    return TRUE;
}

char *
file_to_data (const char *path,
              gsize      *ret_length,
              GError    **error)
{
    GFile *input_file;
    GFileInputStream *input_file_stream;
    guint img_offset = 0x237;
    char *data = NULL;
    gsize data_length;

    /* Open the dwg file for reading */
    input_file = g_file_new_for_path (path);
    input_file_stream = g_file_read (input_file, NULL, error);
    g_object_unref (input_file);

    if (input_file_stream == NULL)
    {
        return NULL;
    }

    /* Check if the opened dwg file is seekable */
    if (g_seekable_can_seek (G_SEEKABLE (input_file_stream)) != TRUE)
    {
        g_warning ("File is not seekable");
        return NULL;
    }

    if (get_png_thumbnail (input_file_stream, img_offset, &data, &data_length, error) == TRUE)
    {
        *ret_length = data_length;
    }
    else
    {
        g_input_stream_close (G_INPUT_STREAM (input_file_stream), NULL, NULL);
        g_object_unref (input_file_stream);
        printf("Error: Unknown thumbnail not found or not available\n");
    }
    return data;
}

